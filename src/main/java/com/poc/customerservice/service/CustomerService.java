package com.poc.customerservice.service;

import com.poc.customerservice.dto.CustomerRequestDTO;
import com.poc.customerservice.dto.CustomerResponseDTO;
import com.poc.customerservice.entity.Customer;
import com.poc.customerservice.mapper.CustomerMapper;
import com.poc.customerservice.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerService implements ICustomerService {

    private CustomerRepository customerRepository;

    private CustomerMapper customerMapper;

    public CustomerService(CustomerRepository customerRepository, CustomerMapper customerMapper) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public List<CustomerResponseDTO> getCustomers() {

        List<Customer> customers = customerRepository.findAll();

        return customers.stream().map(customer ->
                customerMapper.customerToCustomerResponseDTO(customer)).collect(Collectors.toList());
    }

    @Override
    public CustomerResponseDTO addCustomer(CustomerRequestDTO customerRequestDTO) {

        Customer c = customerMapper.customerRequestDTOCustomer(customerRequestDTO);
        customerRepository.save(c);

        return customerMapper.customerToCustomerResponseDTO(c);
    }

    @Override
    public CustomerResponseDTO getCustomerById(Long customerId) {
        Customer c = customerRepository.findById(customerId).orElse(null);
        return customerMapper.customerToCustomerResponseDTO(c);
    }

    @Override
    public CustomerResponseDTO updateCustomer(CustomerRequestDTO customerRequestDTO) {
        Customer updatedCustomer = customerMapper.customerRequestDTOCustomer(customerRequestDTO);
        customerRepository.save(updatedCustomer);

        return customerMapper.customerToCustomerResponseDTO(updatedCustomer);
    }

    @Override
    public void deleteCustomer(Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
